Sketch
========================
Available at [https://sketch.riot.ai](https://sketch.riot.ai)

Sketchpad app using [Blockstack](https://blockstack.org/), HTML5 canvas to draw using touch or mouse, works on iOS, Android, Desktop Browser.
App uses touch events, MSPointer events and mouse events to support iOS, Android, Window Phone and desktop browser.


Fork of [krisrak/html5-canvas-drawing-app] (http://github.com/krisrak/html5-canvas-drawing-app) 

