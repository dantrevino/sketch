var ctx,
  color = "#000";
const blockstack = window.blockstack;
var user = null;
STORAGE_FILE = "sketch.png";

$(document).ready(function() {
  const appConfig = new blockstack.AppConfig();
  const userSession = new blockstack.UserSession({ appConfig: appConfig });

  $("#landing").toggle(!userSession.isUserSignedIn());
  $("#app").toggle(userSession.isUserSignedIn());
  // $('#panel').click(function() {
  //   $('#quickviewDefault').addClass('is-active')
  // })

  if (userSession.isUserSignedIn()) {
    this.login = true;
    this.userData = userSession.loadUserData();
    this.user = new blockstack.Person(this.userData.profile);
    this.user.username = this.userData.username;
    $("#avatar").attr("src", this.user.avatarUrl());
    $("#username").text(this.user.name());
  } else if (userSession.isSignInPending()) {
    userSession.handlePendingSignIn().then(userData => {
      window.location = window.location.origin;
    });
  }

  $("#signinbtn").click(function(e) {
    var origin = window.location.origin + "/";
    console.log(origin);
    var manifest = origin + "manifest.json";
    console.log(manifest);
    userSession.redirectToSignIn(origin, manifest, [
      "store_write",
      "publish_data"
    ]);
  });

  $("#signoutbtn").click(function(e) {
    userSession.signUserOut(window.location.href);
  });

  $("#savebtn").click(function(e) {
    var cnvs = $("#canvas")[0];
    var mypicture = cnvs.toDataURL();
    const encrypt = true;
    userSession
      .putFile(STORAGE_FILE, mypicture, encrypt)
      .then(
        function() {
          Bulma.create("notification", {
            body: "Save successful...",
            parent: document.getElementById("dismissable-notification"),
            isDismissable: true,
            destroyOnDismiss: false,
            color: "info",
            dismissInterval: 1500
          }).show();
        },
        function() {
          Bulma.create("notification", {
            body: "Error saving picture",
            parent: document.getElementById("dismissable-notification"),
            isDismissable: true,
            destroyOnDismiss: false,
            color: "danger",
            dismissInterval: 3000
          }).show();
        }
      )
      .catch(function() {
        Bulma.create("notification", {
          body: "Error saving picture",
          parent: document.getElementById("dismissable-notification"),
          isDismissable: true,
          destroyOnDismiss: false,
          color: "danger",
          dismissInterval: 3000
        }).show();
      });
  });

  $("#loadbtn").click(function(e) {
    Bulma.create("notification", {
      body: "Load function coming soon...",
      parent: document.getElementById("dismissable-notification"),
      isDismissable: true,
      destroyOnDismiss: false,
      color: "warning",
      dismissInterval: 3000
    }).show();
  });

  // setup a new canvas for drawing wait for device init
  setTimeout(function() {
    newCanvas();
  }, 1000);

  // reset palette selection (css) and select the clicked color for canvas strokeStyle
  $(".palette").click(function() {
    $(".palette").css("border-color", "#777");
    $(".palette").css("border-style", "solid");
    $(this).css("border-color", "#fff");
    $(this).css("border-style", "dashed");
    color = $(this).css("background-color");
    $("#quickviewDefault").removeClass("is-active");
    ctx.beginPath();
    ctx.strokeStyle = color;
  });

  // link the new button with newCanvas() function
  $("#new").click(function() {
    newCanvas();
  });
});

// function to setup a new canvas for drawing
function newCanvas() {
  //define and resize canvas
  $("#content").height($(window).height() - 90);
  var canvas =
    '<canvas id="canvas" width="' +
    $(window).width() +
    '" height="' +
    ($(window).height() - 90) +
    '"></canvas>';
  $("#content").html(canvas);

  // setup canvas
  ctx = document.getElementById("canvas").getContext("2d");
  ctx.strokeStyle = color;
  ctx.lineWidth = 5;

  // setup to trigger drawing on mouse or touch
  $("#canvas").drawTouch();
  $("#canvas").drawPointer();
  $("#canvas").drawMouse();
}

// prototype to	start drawing on touch using canvas moveTo and lineTo
$.fn.drawTouch = function() {
  var start = function(e) {
    e = e.originalEvent;
    ctx.beginPath();
    x = e.changedTouches[0].pageX;
    y = e.changedTouches[0].pageY - 44;
    ctx.moveTo(x, y);
  };
  var move = function(e) {
    e.preventDefault();
    e = e.originalEvent;
    x = e.changedTouches[0].pageX;
    y = e.changedTouches[0].pageY - 44;
    ctx.lineTo(x, y);
    ctx.stroke();
  };
  $(this).on("touchstart", start);
  $(this).on("touchmove", move);
};

// prototype to	start drawing on pointer(microsoft ie) using canvas moveTo and lineTo
$.fn.drawPointer = function() {
  var start = function(e) {
    e = e.originalEvent;
    ctx.beginPath();
    x = e.pageX;
    y = e.pageY - 44;
    ctx.moveTo(x, y);
  };
  var move = function(e) {
    e.preventDefault();
    e = e.originalEvent;
    x = e.pageX;
    y = e.pageY - 44;
    ctx.lineTo(x, y);
    ctx.stroke();
  };
  $(this).on("MSPointerDown", start);
  $(this).on("MSPointerMove", move);
};

// prototype to	start drawing on mouse using canvas moveTo and lineTo
$.fn.drawMouse = function() {
  var clicked = 0;
  var start = function(e) {
    clicked = 1;
    ctx.beginPath();
    x = e.pageX;
    y = e.pageY - 44;
    ctx.moveTo(x, y);
  };
  var move = function(e) {
    if (clicked) {
      x = e.pageX;
      y = e.pageY - 44;
      ctx.lineTo(x, y);
      ctx.stroke();
    }
  };
  var stop = function(e) {
    clicked = 0;
  };
  $(this).on("mousedown", start);
  $(this).on("mousemove", move);
  $(window).on("mouseup", stop);
};
